// euler3.cpp
// Created: May 4, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 3
http://projecteuler.net/problem=3
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
*/
// SOLVED 5/6/2012: 6857

#include <iostream>
#include <stdint.h>
#include <time.h>

using namespace std;

int var;

int main()
{
	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	uint64_t n = 600851475143;		// 600,851,475,143
	uint64_t divisor = 2;

	while(divisor <= (n/divisor))
	{
		if (n % divisor == 0)
		{
			//cout << divisor << endl;
			n /= divisor;
		}
		else
		{
			++divisor;
		}
	}

	// SOLUTION END
	final = clock();

	cout << n << endl;
	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	cin >> var;
	return 0;
}