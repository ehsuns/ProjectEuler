#include <iostream>

class Path
{
public:
	static const int LatticeSize = 20;
	static const int NumSteps = LatticeSize * 2;
	Path::Path() : numUps(0), numLefts(0), currPos(0)
	{
		memset(stepArray, 0, sizeof(stepArray));
	}

	Path::~Path(){}

	void UpStep()
	{
		stepArray[currPos++] = UP;
		++numUps;
	}
	void LeftStep()
	{
		stepArray[currPos++] = LEFT;
		++numLefts;
	}

	int GetNumUps(){ return numUps; }
	int GetNumLefts(){ return numLefts; }
	int GetCurrPos(){ return currPos; }

private:
	enum Step { UP = 1, LEFT = 2 };
	int stepArray[NumSteps];
	int numUps;
	int numLefts;
	int currPos;
};