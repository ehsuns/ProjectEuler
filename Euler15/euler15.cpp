// euler15.cpp
// Created: 12/22/2012 6:53:27 PM
// By: Ehsun

/*
Project Euler - Problem 15
http://projecteuler.net/problem=15
Starting in the top left corner of a 2�2 grid, there are 6 routes (without backtracking) to the bottom right corner.
How many routes are there through a 20�20 grid?
*/
// SOLVED 2012.12.23: 137,846,528,820

/*
EXPLANATION:
Properties:
	a)	Without backtracking, number of steps per route is height of lattice times 2.  
		For example, a 2x2 lattice has max 4 steps per route to get to bottom right corner.  
		3x3 lattice has 6 steps per route. 
	b)	Only two directions possible per step, RIGHT and DOWN.  
	c)	For each route, number of RIGHT steps must equal DOWN steps.

Solution Idea:
	Given the above properties, the solution is just to find the number of permutations 
	of RIGHT and DOWN steps per route.  
	This requires recursion.  The smallest case being a single lattice with two step paths.
	R-D and D-R.  Return sets of combinations, and append R at the beginning and D at 
	the end of each.  Then append D at the beginning and R at the end of each.  Then return
	that set, etc.

	UPDATE: So that was a bad idea.  Among other problems (such as speed), the solution 
	doesn't work for palindromic paths (Ex. RDDR).  Eventualy I abandoned the idea of 
	recursion and did it usinga for loop.
	I wrote a clas Path to help with tracking.  Used UP and LEFT, as a means of starting 
	from the bottom right and moving backwards.  This works for small sizes of the lattice
	but eventually runs out of memory since I store every single path and keep adding to it.
	In the end I used this method to get the number of paths for size 2, 3, 4, 5, and 6
	lattices.  Using pen and paper I characterized a mathematical patter, then put it to 
	use in code.

	Lattice Size		Num Paths		The Pattern
	1					2				
	2					6				thisNumPaths = lastNumPaths * 3
	3					20				thisNumPaths = lastNumPaths * 3.3333
	4					70				thisNumPaths = lastNumPaths * 3.5
	5					252				thisNumPaths = lastNumPaths * 3.6
	6					924				thisNumPaths = lastNumPaths * 3.6666

	See the pattern in the multiplier?
	The multiplier increases with each lattice size by 1/(n).

	So for lattice size 20, there are over 100 Billion paths.
	Maybe it wasn't running out of memory before.  Maybe some of the code just couldn't 
	handle the number.  A 32 bit field can't hold that big of a number.
*/

#include <iostream>
#include <stdint.h>
#include <time.h>
#include <vector>
#include <string>
#include <algorithm>
#include "Path.h"

using namespace std;

#define MAP_ROUTES 0	// Only works for smaller sizes of Path::LatticeSize

int main()
{
	clock_t init, final;
	
	init = clock();
	// SOLUTION BEGIN
#if MAP_ROUTES
	vector<Path> allRoutes;
	Path branchUp;
	Path branchLeft;
	branchUp.UpStep();
	branchLeft.LeftStep();
	allRoutes.push_back(branchUp);
	allRoutes.push_back(branchLeft);
	for(int n = 0; n < Path::NumSteps-1; ++n)
	{
		vector<Path> newSet;
		for(Path& thisPath : allRoutes)
		{
			if (thisPath.GetNumUps() >= Path::LatticeSize)
			{
				Path branchLeft = thisPath;
				branchLeft.LeftStep();
				newSet.push_back(branchLeft);
			}
			else if (thisPath.GetNumLefts() >= Path::LatticeSize)
			{
				Path branchUp = thisPath;
				branchUp.UpStep();
				newSet.push_back(branchUp);
			}
			else
			{
				Path branchUp = thisPath;
				Path branchLeft = thisPath;
				branchUp.UpStep();
				branchLeft.LeftStep();
				newSet.push_back(branchUp);
				newSet.push_back(branchLeft);
			}
		}
		allRoutes = newSet;
	}
	int64_t lastNumRoutes = allRoutes.size();
#else // Calculate Routes
	int64_t lastNumRoutes = 6;
	int64_t thisNumRoutes = 6;
	double multiplier = 3.0;
	double denom = 1.0;
	for (int n = 3; n <= Path::LatticeSize; ++n)
	{
		denom += n - 1;
		multiplier = multiplier + (1/denom);
		double thisNumRoutes = lastNumRoutes*multiplier;
		lastNumRoutes = thisNumRoutes + .5;
	}
#endif
	// SOLUTION END
	final = clock();
	cout << "For a lattice of size " << Path::LatticeSize << " the number of unique paths: " << lastNumRoutes << endl;
	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	int var;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}