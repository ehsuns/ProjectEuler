// euler9.cpp
// Created: May 18, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 9
http://projecteuler.net/problem=9
A Pythagorean triplet is a set of three natural numbers, a  b  c, for which,
a^2 + b^2 = c^2
For example, 32 + 42 = 9 + 16 = 25 = 52.
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*/
// SOLVED 5/18/2012: 31875000

#include <iostream>
#include <stdint.h>
#include <time.h>

using namespace std;

#define	MAX	500
int var;

/*
Three varialbes to play with here: a, b, c.  The basic concept is to try every single integer permutation that yields true for the pythagorean theorem, yet produces a sum of 1000 or less.
This produces three forloops, one nested in the next, that goes from 1 to 1000.
Now take out unnecessary operations: 
 - We can start each nested forloop at the current value of the nesting forloop.  Explained below in comments.
 - We can break out of the looop if the sum is ever greater than 1000.
 - The max for each loop is 500.  We know this because the sum should be 1000 and if any of the variables goes about 500 it would mean that one of the other variables would have to be negative to produce a sum of 1000.

Could be better ways to do this.  
*/
int main()
{
	bool isTriple = false;
	int a, b, c;
	int a2, b2, c2;
	int theA, theB, theC;

	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	for (a = 1; a <= MAX; a++)					// None of the triples can be 0, so start at 1.
	{
		for (b = a; b <= MAX; b++)				// Start this loop at the value of a.  Otherwise we repeat permutations: a = 1, b = 5 is the same as a = 5 and b = 1;
		{
			for (c = b; c <= MAX; c++)			// We can start this loop at the value of b because we know that c MUST be larger than b and a, due to the pythagorean theorem a^2 + b^2 = c^2.
			{
				if ((a + b + c) > 1000)			// If the sum is greater than 1000, break out of the loop.  No reason to count up the rest of the values.
					break;

				a2 = a * a;
				b2 = b * b;
				c2 = c * c;	

				if ((a2 + b2) == c2)
				{
					isTriple = true;
				}
				else
				{
					isTriple = false;
				}

				if ((isTriple) && ((a + b + c) == 1000))
				{
					theA = a;
					theB = b;
					theC = c;
					/*
					cout << "a: " << a << endl;
					cout << "b: " << b << endl;
					cout << "c: " << c << endl;
					cout << "Product: " << a * b * c << endl
					*/
				}
			}
		}
	}

	// SOLUTION END
	final = clock();

	cout << "a: " << theA << endl;
	cout << "b: " << theB << endl;
	cout << "c: " << theC << endl;
	cout << "Product: " << theA * theB * theC << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}