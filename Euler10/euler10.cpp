// euler10.cpp
// Created: May 18, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 10
http://projecteuler.net/problem=10
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
*/
// SOLVED 5/18/2012: 142913828922

#include <iostream>
#include <stdint.h>
#include <time.h>

using namespace std;

int var;

/*
EXPLANATION:

*/
int main()
{
	bool isPrime;
	int divisor;
	int n;
	
	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	uint64_t sum = 2 + 3 + 5 + 7 + 11 + 13;
	for (int n = 17; n <= 2000000; n += 2)
	{
		int num = n;
		isPrime = true;
		for (divisor = 3; divisor <= num/divisor; divisor += 2)
		{
			if (num % divisor == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			//cout << n << endl;
			sum += n;
		}
	}
	// SOLUTION END
	final = clock();

	cout << "Sum: " << sum << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}