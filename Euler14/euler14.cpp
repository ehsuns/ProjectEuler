// euler14.cpp
// Created: 11/11/2012 9:37:23 PM
// By: Ehsun

/*
Project Euler - Problem 14
http://projecteuler.net/problem=14

The following iterative sequence is defined for the set of positive integers:
n -> n/2 (n is even)
n -> 3n + 1 (n is odd)
Using the rule above and starting with 13, we generate the following sequence:
13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
Which starting number, under one million, produces the longest chain?
NOTE: Once the chain starts the terms are allowed to go above one million.
*/

// SOLVED 2012.11.11:  	837799

/*
EXPLANATION:
Started with a brute force approach, just to get started.  Took about 2.5 sec for algorithm to run.
Then started optimizing:
	- Use right-shift instead of explicit divide-by-2.
	- Operate only on odd numbers, since operating on even starting numbers will map down to a previously calculated odd number.
	- Use a lookup table to store calculated values.  When a result is less than starting number, and is odd, then use the stored value in look up table to get chains.

Total time came down to about 88 ms.
*/

#include <iostream>
#include <stdint.h>
#include <time.h>

using namespace std;

#define RECURSION	1

const int64_t STARTNUM = 3;
const int64_t ENDNUM = 1000000;
int64_t previousChains[ENDNUM];
int greatestChain = 0;
int greatestStart = 0;

#if !RECURSION
int chain = 1;
#else
int RunBranch(int64_t num)
{
	int chain = 0;
	int64_t result = num;

	if (num == 1)
		return 1;

	// Use a lookup table to check against previous numbers.  No need to rerun calculations we already have.
	if (result < ENDNUM)
	{
		if (previousChains[result - 1] != 0)
		{
			chain = previousChains[result - 1];
			return chain;
		}
	}

	// If number does not exist in lookup table, then run calculation.
	if ((result & 1) == 0)		
	{
		result = result >> 1;
	}
	else
	{
		result = (3 * result) + 1;
	}
		
	chain += RunBranch(result) + 1;

	if (num < ENDNUM)
	{
		if (previousChains[num - 1] == 0)
		{
			previousChains[num - 1] = chain;
		}
	}

	return chain;
}
#endif

int main()
{
	clock_t init, final;

	if (STARTNUM < 2)
	{
		cout << "STARTNUM must be at least 2." << endl;
		return -1;
	}

	init = clock();
	// SOLUTION BEGIN
	previousChains[0] = 1;
	for (int startNumber = STARTNUM; startNumber < ENDNUM; startNumber += 2)	// Only deal with odd numbers.  Even start numbers will map down to a previous odd number anwyays.
	{ 

#if RECURSION
		int chain = RunBranch(startNumber);
#else
		int64_t result = startNumber;
		while (result != 1)
		{
			// Use a lookup table to check against previous numbers.  No need to rerun calculations we already have.
			if (result < startNumber)
			{
				if (previousChains[result - 1] != 0)		// We are not operating on even numbers so we have to handle this case.
				{
					chain += previousChains[result - 1] - 1;
					break;
				}
			}
			// If number does not exist in lookup table, then run calculation.
			if ((result & 1) == 0)			
			{
				result = result >> 1;
			}
			else
			{
				result = (3 * result) + 1;
			}
			++chain;
		}
		previousChains[startNumber - 1] = chain;
#endif
		
		if (chain > greatestChain)
		{
			greatestChain = chain;
			greatestStart = startNumber;
			//cout << "Starting number: " << greatestStart << ".  Chain length: " << greatestChain << endl;
		}
		chain = 1;
	}

	// SOLUTION END
	final = clock();
	
	cout << "Starting number: " << greatestStart << ".  Chain length: " << greatestChain << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	int var;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}