#include "GridVector.h"

GridVector::GridVector()
{
	x = 0;
	y = 0;
}

GridVector::GridVector(uint8_t inX, uint8_t inY)
{
	x = inX;
	y = inY;
}

GridVector GridVector::operator+(const GridVector& otherVector) const
{
	GridVector result;

	result.x = x + otherVector.x;
	result.y = y + otherVector.y;

	return result;
}

void GridVector::Set(uint8_t inX, uint8_t inY)
{
	x = inX;
	y = inY;
}

bool GridVector::InRange()
{
	if ((x> ROWMAX) || (y > COLMAX))
		return false;
	
	return true;
}