#include "GridVector.h"

class Euler11Helper
{
private:
	enum Direction
	{
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouthWest,
		West,
		NorthWest
	};

	Euler11Helper(Direction dir);
	int GetProduct();
	void Step();
	void Step(Direction dir);

#if 0	// Why cant i do this?
	const GridVector someVector(1,1);
#endif

	GridVector m_first;
	GridVector m_second;
	GridVector m_third;
	GridVector m_fourth;
	Direction m_dir;
};