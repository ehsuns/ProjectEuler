#include <stdint.h>

class GridVector
{
	public:
		static const uint8_t ROWMAX = 20;
		static const uint8_t COLMAX = 20;
		uint8_t x;
		uint8_t y;

		GridVector();
		GridVector(uint8_t inX, uint8_t inY);
		GridVector operator+(const GridVector& otherVector) const;
		void Set(uint8_t inX, uint8_t inY);
		bool InRange();
};
