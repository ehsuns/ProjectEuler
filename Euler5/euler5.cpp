// euler5.cpp
// Created: May 6, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 5
http://projecteuler.net/problem=5
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/
// SOLVED 5/6/2012: 232792560

#include <iostream>
#include <time.h>

using namespace std;

int var;

int main()
{
	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	bool divisible = false;
	int multiple = 20;

	while(!divisible)
	{
		for (int i = 1; i < 20; i++)
		{
			if (multiple % i != 0)
			{
				divisible = false;
				multiple += 20;
				break;
			}
			else
			{
				divisible = true;
			}
		}
	}

	// SOLUTION END
	final = clock();

	cout << multiple << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	cin >> var;
	return 0;
}