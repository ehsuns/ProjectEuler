// euler6.cpp
// Created: May 6, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 6
http://projecteuler.net/problem=6
The sum of the squares of the first ten natural numbers is, 12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is, (1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.
Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
*/
// SOLVED 5/6/2012: 25164150

#include <iostream>
#include <time.h>

using namespace std;

int var;

int main()
{
	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	int sumSquare = 0;	// Sum of the squares;
	int squareSum = 0;	// Square of the sum;

	for (int i = 1; i <= 100; i++)
	{
		sumSquare += i*i;
		squareSum += i;
	}
	
	squareSum = squareSum * squareSum;
	// SOLUTION END
	final = clock();

	cout << squareSum - sumSquare << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;

	cin >> var;
	return 0;
}