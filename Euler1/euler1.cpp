// euler1.cpp
// Created: May 4, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 1
http://projecteuler.net/problem=1
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
*/
// SOLVED 5/4/2012: 233168

/*
EXPLANATION:
Brute force solution.  Run through every number up to N, check to see it's divisible by 3 or 5, and add it to the sum if it is.
I added a USE_FOR_EACH_AND_LAMBDA define just to use STL algorithms and lambdas for fun.
*/
#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>

using namespace std;

#define USE_FOR_EACH_AND_LAMBDA		1

#define	N		1000

int var;

int main()
{
	clock_t init, final;

#if USE_FOR_EACH_AND_LAMBDA
	// init the vectors
	vector<int> theIntegers;
	for (int i = 0; i < N; ++i)
	{
		theIntegers.push_back(i);
	}
#else
	unsigned int sum = 0;
#endif

	init = clock();
	// SOLUTION BEGIN
#if USE_FOR_EACH_AND_LAMBDA
	int sum = 0;
	for_each(begin(theIntegers), end(theIntegers), [&](int i) {
		if ((i % 3 == 0) || (i % 5 == 0))
		{
			sum += i;
		}
	});
#else
	for (int i = 0; i < N; i++)
	{
		if ((i % 3 == 0) || (i % 5 == 0))
		{
			sum += i;
		}
	}
#endif
	// SOLUTION END
	final = clock();
	
	cout << "The sum is: " << sum << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	int var;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}