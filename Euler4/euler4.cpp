// euler4.cpp
// Created: May 6, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 4
http://projecteuler.net/problem=4
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 99.
Find the largest palindrome made from the product of two 3-digit numbers.
*/
// SOLVED 5/6/2012: 906609

#include <iostream>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int var;

int main()
{
	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	uint8_t digits[6];		// Largest number for 3-digit multiplication can only be 6 digits long.
	bool palindrome = false;
	int result;
	int max = 0;
	int num;
	int nDigits = 0;

	for (int i = 999; i >= 100; i--)
	{
		for (int j = 999; j >= 100; j--)
		{
			nDigits = 0;
			result = i * j;
			num = result;
			for (int k = 100000; k >= 1; k /= 10)		// Throw digits into an array for comparison.
			{
				if ((num/k > 0) || (nDigits > 0))
				{
					digits[nDigits++] = num/k;
					num = num % k;
				}
			}

			int index = 0;
			--nDigits;
			while (index <= nDigits)					// March one index forward, and one index backwards.  Compare as you go along.
			{
				if (digits[index] == digits[nDigits])
				{
					palindrome = true;
					++index;
					--nDigits;
				}
				else 
				{
					palindrome = false;
					break;
				}
			}

			if ((palindrome) && (result > max))			// Check for max
			{
				max = result;
			}
		}
	}

	// SOLUTION END
	final = clock();

	cout << max << endl;
	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	cin >> var;
	return 0;
}