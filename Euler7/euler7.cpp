// euler7.cpp
// Created: May 6, 2012
// By: Ehsun Siddiqui

/*
Project Euler - Problem 7
http://projecteuler.net/problem=7
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
What is the 10 001st prime number?
*/
// SOLVED 5/6/2012: 104743

#include <iostream>
#include <stdint.h>
#include <time.h>

using namespace std;

int var;

int main()
{
	clock_t init, final;
	init = clock();
	// SOLUTION BEGIN
	int primeth = 0;
	uint64_t n = 0;
	uint64_t divisor = 2;
	uint64_t thePrime = 0;

	n = 2;
	while (primeth < 10001)
	{
		uint64_t nUnderTest = n;
		bool isPrime = true;
		divisor = 2;
		while(divisor <= (nUnderTest/divisor))
		{
			if (nUnderTest % divisor == 0)
			{
				nUnderTest = nUnderTest/divisor;
				isPrime = false;
				break;
			}
			else
			{
				divisor++;
			}
		}

		if (isPrime)
		{
			++primeth;
			thePrime = n;
			//cout << n << endl;
		}

		++n;
	}
	// SOLUTION END
	final = clock();
	cout << thePrime << endl;
	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	cin >> var;
	return 0;
}