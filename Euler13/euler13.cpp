// euler13.cpp
// Created: 11/11/2012 2:07:16 AM
// By: Ehsun

/*
Project Euler - Problem 13
http://projecteuler.net/problem=13
Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
(See number list in header file.)
*/
// SOLVED 2012.11.11: 5537376230

#include <iostream>
#include <stdint.h>
#include <time.h>
#include <list>
#include "largeNums.h"

using namespace std;

/*
EXPLANATION:

*/

int main()
{
	clock_t init, final;
	
	init = clock();
	// SOLUTION BEGIN
	list<int> firstTen;
	int currDigit;
	int digitToList;
	int theCarry = 0;
	for (currDigit = N_DIGITS - 1; currDigit >= 0; --currDigit)
	{
		int thisSum = theCarry;
		for (int n = currDigit; n < sizeof(numBuff) - 1; n += N_DIGITS)
		{
			thisSum += numBuff[n] - '0';
		}

		digitToList = thisSum % 10;

		theCarry = thisSum / 10;

		firstTen.push_front(digitToList);
	}

	digitToList = theCarry;
	while (digitToList >= 10)
	{
		firstTen.push_front(digitToList % 10);
		digitToList /= 10;
	}
	firstTen.push_front(digitToList);

	// SOLUTION END
	final = clock();
	
	cout << "Sum of the numbers: ";
	list<int>::iterator i;
	for (i = firstTen.begin(); i != firstTen.end(); ++i)
	{
		cout << *i;
	}
	cout << endl;

	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	int var;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}