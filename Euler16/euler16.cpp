// euler16.cpp
// Created: 12/23/2012 8:49:58 PM
// By: Ehsun

/*
Project Euler - Problem 16
http://projecteuler.net/problem=16

2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
What is the sum of the digits of the number 2^1000?
*/
// SOLVED YYYY.MM.DD: 

#include <iostream>
#include <cstdint>
#include <time.h>

using namespace std;

/*
EXPLANATION:

*/

int main()
{
	clock_t init, final;
	
	init = clock();
	// SOLUTION BEGIN

	double TwoTo1000 = pow(2, 1000);

	cout << TwoTo1000 << endl;
	
	// SOLUTION END
	final = clock();
	
	cout << endl;
	cout << "Total Time: " << (double)(final - init) << " ms" << endl;
	int var;
	cin >> var;		// Prevent console from closing at end.
	return 0;
}